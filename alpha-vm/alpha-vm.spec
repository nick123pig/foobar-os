Name: alpha-vm
Version: 0.0.1
Release: 1%{?dist}
# Release:    %autorelease
Summary: Alpha VM
License: MIT
URL: https://idtec.com
Source0: %{name}.tar

BuildRequires:  mkosi = 13
BuildRequires:  python3.11
BuildRequires:  pesign

%description
This package is the %{name} vm image

%global debug_package %{nil}

%prep
%autosetup -n %{name}

%build
python3.8 -m mkosi build

%install
install -Dpm 0644 %{name}.raw %{buildroot}%{_datadir}/isos/%{name}.raw

%files
%{_datadir}/isos/%{name}.raw

