#!/bin/bash
set -euxo pipefail

function add_koji_tag {
    # $1 is new tag / $2 is tag to inherit
    if koji list-tags | grep -q $1; then
        echo "$1 already added to koji; skipping"
    else
        koji add-tag --parent $2 $1
        koji add-tag --parent $1  --arches=x86_64 $1-build
        koji edit-tag $1-build -xmock.yum.module_hotfixes=1
        koji add-target $1 $1-build
        koji add-group $1-build build
        koji add-group $1-build srpm-build
        koji add-group-pkg $1-build build tar gcc-c++ redhat-rpm-config redhat-release which xz sed make bzip2 gzip gcc coreutils unzip shadow-utils diffutils cpio bash gawk rpm-build info patch util-linux findutils grep
        koji add-group-pkg $1-build srpm-build bash curl cvs gnupg2 make redhat-rpm-config rpm-build shadow-utils
        koji add-external-repo -t $1-build $1-baseos http://zz.stocchero.net:8080/rpms/8.8/rhel-8-for-\\\$arch-baseos-rpms/
        koji add-external-repo -t $1-build $1-baseos-srpms http://zz.stocchero.net:8080/rpms/8.8/rhel-8-for-\\\$arch-baseos-source-rpms/
        koji add-external-repo -t $1-build $1-appstream http://zz.stocchero.net:8080/rpms/8.8/rhel-8-for-\\\$arch-appstream-rpms/
        koji add-external-repo -t $1-build $1-appstream-srpms http://zz.stocchero.net:8080/rpms/8.8/rhel-8-for-\\\$arch-appstream-source-rpms/
        koji add-external-repo -t $1-build $1-codeready http://zz.stocchero.net:8080/rpms/8.8/codeready-builder-for-rhel-8-\\\$arch-rpms/
        koji add-external-repo -t $1-build $1-epel http://zz.stocchero.net:8080/rpms/8.8/epel/
        # koji regen-repo $1-build
    fi
}
