Name: foo
Version: 1.0.3
Release: 1%{?dist}
# Release:    %autorelease
Summary: foo
License: MIT License
URL: https://foo.com
Source0: %{name}.tar

%description
This package is %{name}

%global debug_package %{nil}

%prep
%autosetup -n %{name}

%build

%install
touch foo
install -Dpm 0644 foo %{buildroot}%{_datadir}/foo

%files
%{_datadir}/foo