Name: beta
Version: 1.0.3
Release: 1%{?dist}
# Release:    %autorelease
Summary: beta
License: MIT License
URL: https://beta.com
Source0: %{name}.tar
BuildArch: noarch

%description
This package is %{name}

%global debug_package %{nil}

%prep
%autosetup -n %{name}

%build

%install
touch beta
install -Dpm 0644 beta %{buildroot}%{_datadir}/beta

%files
%{_datadir}/beta