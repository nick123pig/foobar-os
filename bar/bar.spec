Name: bar
Version: 1.0.3
Release: 1%{?dist}
# Release:    %autorelease
Summary: bar
License: MIT License
URL: https://bar.com
Source0: %{name}.tar
BuildArch: noarch
BuildRequires: foo

%description
This package is %{name}

%global debug_package %{nil}

%prep
%autosetup -n %{name}

%build

%install
touch bar
install -Dpm 0644 bar %{buildroot}%{_datadir}/bar

%files
%{_datadir}/bar