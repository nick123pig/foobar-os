%global python3_pkgversion 3.11
Name:           mkosi
Version:        13
Release: 1%{?dist}
# Release:    %autorelease
Summary:        MKOSI for rhel 8

License:        MIT
URL:            https://github.com/systemd/mkosi
Source:         %{url}/archive/refs/tags/v%{version}.tar.gz

BuildArch:      noarch
Requires:       pesign
BuildRequires:  python%{python3_pkgversion}-devel
BuildRequires:  python%{python3_pkgversion}-setuptools

Patch0: v13backend.patch
Patch1: v13init.patch

%description 
patched mkosi for rhel

%prep
%autosetup -n %{name}-%{version} -v -p0

%build
python%{python3_pkgversion} setup.py build

%install
ls -lah
python%{python3_pkgversion} setup.py install --skip-build --root %{buildroot}
install -D bin/mkosi -t %{buildroot}%{_bindir}

%files
%{_bindir}/mkosi
/usr/lib/python%{python3_pkgversion}/site-packages/mkosi/
/usr/lib/python%{python3_pkgversion}/site-packages/mkosi-*.egg-info/
/usr/share/man/man1/mkosi.1.gz
