#!/bin/bash
set -euxo pipefail

declare BASE_TAG=base
declare MAIN_TAG=master
declare DEV_TAG=dev

source ./add-koji-tag.sh

if koji list-tags | grep -q $BASE_TAG; then
  echo "$BASE_TAG already added to koji; skipping"
else
    koji add-tag $BASE_TAG
    koji add-tag --parent $BASE_TAG  --arches=x86_64 $BASE_TAG-build
    koji edit-tag $BASE_TAG-build -xmock.yum.module_hotfixes=1
    koji add-target $BASE_TAG $BASE_TAG-build
    koji add-group $BASE_TAG-build build
    koji add-group $BASE_TAG-build srpm-build
    koji add-group-pkg $BASE_TAG-build build tar gcc-c++ redhat-rpm-config redhat-release which xz sed make bzip2 gzip gcc coreutils unzip shadow-utils diffutils cpio bash gawk rpm-build info patch util-linux findutils grep
    koji add-group-pkg $BASE_TAG-build srpm-build bash curl cvs gnupg2 make redhat-rpm-config rpm-build shadow-utils
    koji add-external-repo -t $BASE_TAG-build $BASE_TAG-baseos http://zz.stocchero.net:8080/rpms/8.8/rhel-8-for-\$arch-baseos-rpms/
    koji add-external-repo -t $BASE_TAG-build $BASE_TAG-baseos-srpms http://zz.stocchero.net:8080/rpms/8.8/rhel-8-for-\$arch-baseos-source-rpms/
    koji add-external-repo -t $BASE_TAG-build $BASE_TAG-appstream http://zz.stocchero.net:8080/rpms/8.8/rhel-8-for-\$arch-appstream-rpms/
    koji add-external-repo -t $BASE_TAG-build $BASE_TAG-appstream-srpms http://zz.stocchero.net:8080/rpms/8.8/rhel-8-for-\$arch-appstream-source-rpms/
    koji add-external-repo -t $BASE_TAG-build $BASE_TAG-codeready http://zz.stocchero.net:8080/rpms/8.8/codeready-builder-for-rhel-8-\$arch-rpms/
    koji add-external-repo -t $BASE_TAG-build $BASE_TAG-epel http://zz.stocchero.net:8080/rpms/8.8/epel/
fi

if koji list-tags | grep -q $MAIN_TAG; then
  echo "$MAIN_TAG already added to koji; skipping"
else
    add_koji_tag $MAIN_TAG $BASE_TAG
fi

if koji list-tags | grep -q $DEV_TAG; then
  echo "$DEV_TAG already added to koji; skipping"
else
    add_koji_tag $DEV_TAG $MAIN_TAG
fi

# koji add-tag util
# koji add-tag --parent util --arches=x86_64 util-build
# koji add-tag-inheritance util master
# koji add-target util util-build
# koji add-group util-build build
# koji add-group util-build srpm-build
# koji add-group-pkg util-build build tar gcc-c++ redhat-rpm-config redhat-release which xz sed make bzip2 gzip gcc coreutils unzip shadow-utils diffutils cpio bash gawk rpm-build info patch util-linux findutils grep
# koji add-group-pkg util-build srpm-build bash curl cvs gnupg2 make redhat-rpm-config rpm-build shadow-utils
# koji add-external-repo -t util-build util-baseos http://zz.stocchero.net:8080/rpms/8.8/rhel-8-for-\$arch-baseos-rpms/
# koji add-external-repo -t util-build util-baseos-srpms http://zz.stocchero.net:8080/rpms/8.8/rhel-8-for-\$arch-baseos-source-rpms/
# koji add-external-repo -t util-build util-appstream http://zz.stocchero.net:8080/rpms/8.8/rhel-8-for-\$arch-appstream-rpms/
# koji add-external-repo -t util-build util-appstream-srpms http://zz.stocchero.net:8080/rpms/8.8/rhel-8-for-\$arch-appstream-source-rpms/
# koji add-external-repo -t util-build util-codeready http://zz.stocchero.net:8080/rpms/8.8/codeready-builder-for-rhel-8-\$arch-rpms/
# koji add-external-repo -t util-build util-epel http://zz.stocchero.net:8080/rpms/8.8/epel/